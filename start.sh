#!/bin/bash
app="silence.docker"
docker build -t ${app} .
docker run -it -p 80:80 --name=${app} -v $PWD:/app ${app}
