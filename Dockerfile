FROM python:3.7-alpine

WORKDIR /home/ubuntu/TGS/the-great-silence

COPY requirements.txt requirements.txt

RUN python -m venv venv
RUN venv/bin/pip install -r requirements.txt

ENV STATIC_URL /static
ENV STATIC_PATH /home/ubuntu/TGS/the-great-silence/app/static
EXPOSE 80
COPY . /home/ubuntu/TGS/the-great-silence

CMD [ "venv/bin/python", "./main.py" ]
