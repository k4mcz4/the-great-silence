from app import app
from flask import render_template, redirect, url_for


@app.route('/')
def home():
    return render_template('home.html')


@app.route('/backdoor/admin/characters')
def admin_characters():
    return render_template('admin/characters.html')
